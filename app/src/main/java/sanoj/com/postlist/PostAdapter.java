package sanoj.com.postlist;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class PostAdapter extends ArrayAdapter {

    Activity context;
    ArrayList<Post> data;
    ViewHolder viewHolder;

    public PostAdapter(Activity context, ArrayList<Post> data) {
        super(context, R.layout.adapter_post, data);
        this.context = context;
        this.data = data;
    }

    public static class ViewHolder {
        public CircleImageView profileImage;
        public TextView txtName;
        public TextView txtTime;
        public ImageView postImage;
        public TextView txtDesc;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        final int index = position;

        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.adapter_post, null);

            // configure view holder
            viewHolder = new ViewHolder();
            viewHolder.profileImage = (CircleImageView) rowView.findViewById(R.id.profileImage);
            viewHolder.txtName = (TextView) rowView.findViewById(R.id.txtName);
            viewHolder.txtTime = (TextView) rowView.findViewById(R.id.txtTime);
            viewHolder.postImage = (ImageView) rowView.findViewById(R.id.postImage);
            viewHolder.txtDesc = (TextView) rowView.findViewById(R.id.txtDesc);
            rowView.setTag(viewHolder);
        }

        Post p = data.get(index);
        viewHolder = (ViewHolder) rowView.getTag();
        Picasso.with(this.context).load(p.getUserPhoto()).into(viewHolder.profileImage);
        viewHolder.txtName.setText(p.getUserName());
        viewHolder.txtTime.setText(p.getDate());

        Picasso.with(this.context).load(p.getImageUrl()).into(viewHolder.postImage,
                new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d("image", "loaded successfully");
                    }
                    @Override
                    public void onError() {
                        Log.e("image", "cant load");
                    }
                });
        viewHolder.txtDesc.setText(p.getMessage());

        return rowView;
    }


}
