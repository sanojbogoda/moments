package sanoj.com.postlist;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


interface PostsLoadable {
    void onPostsLoaded(ArrayList<Post> postsArray);
}

public class HttpPosts {

    private Context context;
    private PostsLoadable PostsLoadable;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    public HttpPosts(Context context, PostsLoadable PostsLoadable) {
        this.context = context;
        this.PostsLoadable = PostsLoadable;
        this.pref = context.getSharedPreferences("CACHE", Context.MODE_PRIVATE);
    }

    //api call
    public void getData(String url) {

        Log.d("cache", pref.getString("cache", "empty"));
        if (this.pref.contains("cache")) {
            //send data
            try {
                Log.d("cache", "loading from cache");
                JSONObject data = new JSONObject(pref.getString("cache", "empty"));
                ArrayList<Post> postsArray = parseJson(data);
                PostsLoadable.onPostsLoaded(postsArray);
            }catch (Exception e){
                Log.e("error", e.getMessage());
            }

        } else {

            //loading from server
            RequestQueue queue = Volley.newRequestQueue(this.context);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("cache", "loading from server");

                    //Caching: Save the response in shared preference. Alternatively we can save the json data in SQLite database
                    //Log.d("response", response.toString());
                    editor = pref.edit();
                    editor.putString("cache", response.toString());
                    editor.commit();

                    Log.d("cache", pref.getString("cache", "empty"));

                    //send data
                    ArrayList<Post> postsArray = parseJson(response);
                    PostsLoadable.onPostsLoaded(postsArray);
                    }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    System.out.println("error");
                }
            });
            queue.add(jsObjRequest);
        }
    }


    private ArrayList<Post> parseJson(JSONObject object) {
        ArrayList<Post> postArray = new ArrayList<Post>();
        try {

            JSONObject post;
            JSONArray posts = object.getJSONArray("data");
            Log.d("test", "size : " + posts.length());

            for (int i = 0; i < posts.length(); i++) {
                post = posts.getJSONObject(i);
                System.out.println("message " + post.getString("message"));

                Post p = new Post();
                p.setId(post.getInt("_id") + "");
                p.setMessage(post.getString("message"));
                p.setDate(post.getString("postedAt").substring(0, 10));
                p.setImageUrl(post.getString("imageURL"));

                JSONObject user = post.getJSONObject("user");

                p.setUserId(user.getInt("_id") + "");
                p.setUserName(user.getString("fullName"));
                p.setUserPhoto(user.getString("imageURL"));

                postArray.add(p);
            }

        } catch (JSONException e) {
            Log.e("error", e.getMessage());
        }

        return postArray;
    }






}//end
