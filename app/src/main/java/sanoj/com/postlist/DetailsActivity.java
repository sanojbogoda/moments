package sanoj.com.postlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


public class DetailsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        ImageButton btnBack = (ImageButton) findViewById(R.id.btnBack);
        ImageView postImage = (ImageView) findViewById(R.id.postImage);

        TextView tvName = (TextView) findViewById(R.id.tvName);
        TextView tvDate = (TextView) findViewById(R.id.tvDate);
        TextView tvUser = (TextView) findViewById(R.id.tvName);
        TextView tvDesc = (TextView) findViewById(R.id.tvDesc);

        Intent i = getIntent();
        if(i.hasExtra("clickedPost")) {
            Post post = (Post) i.getSerializableExtra("clickedPost");
            tvName.setText(post.getMessage());
            tvDate.setText(post.getDate());
            tvUser.setText(post.getUserName());
            tvDesc.setText(post.getMessage());
            Picasso.with(this).load(post.getImageUrl()).into(postImage);
        }

        //back
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
