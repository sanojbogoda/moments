package sanoj.com.postlist;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class PostListActivity extends AppCompatActivity implements PostsLoadable, SwipeRefreshLayout.OnRefreshListener {

    private ListView listView;
    private SwipeRefreshLayout refreshLayout;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private ArrayList<Post> postsArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_list);

        ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#888888")));
        bar.setDisplayHomeAsUpEnabled(true);

        this.listView = (ListView) findViewById(R.id.listView);

        this.refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refreshLayout);
        this.refreshLayout.setOnRefreshListener(this);

        this.pref = this.getSharedPreferences("CACHE", Context.MODE_PRIVATE);

        loadPosts();
    }

    //Async http handler
    private void loadPosts(){
        HttpPosts client = new HttpPosts(this, this);
        client.getData("http://thedemoapp.herokuapp.com/post");
    }

    //callback
    public void onPostsLoaded(ArrayList<Post> postsArray) {
        this.postsArray = postsArray;
        if (this.refreshLayout.isRefreshing()) {
            this.refreshLayout.setRefreshing(false);
            Toast.makeText(this, "Post list refreshed!", Toast.LENGTH_SHORT).show();
        }
        this.listView.setAdapter(new PostAdapter(this, postsArray));
        this.listView.setOnItemClickListener(listener);
    }


    private AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d("action", "clicked");

            Post clickedPost = postsArray.get(position);

            Intent i = new Intent(PostListActivity.this, DetailsActivity.class);
            i.putExtra("clickedPost", clickedPost);
            startActivity(i);
        }
    };

    //pull to refresh
    @Override
    public void onRefresh() {
        Log.d("refresh", "refreshing");

        //clear
        editor = pref.edit();
        editor.clear();
        editor.commit();

        loadPosts();
    }


}
